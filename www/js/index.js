/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.getElementById('choose-dice-btn').addEventListener('touchstart', this.showDice, false);
        document.getElementById('choose-card-btn').addEventListener('touchstart', this.showCard, false);
        document.getElementById('roll-dice-btn').addEventListener('touchstart', this.rollDice, false);
        document.getElementById('pick-card-btn').addEventListener('touchstart', this.pickCard, false);
        document.getElementById('form-btn').addEventListener('touchstart', this.submitForm, false);
        document.getElementById('buy-games-btn').addEventListener('touchstart', this.openBrowser, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        console.log('Received Event: ' + id);
    },

    showDice: function() {
        app.rollDice();
        document.getElementById('item-title').innerText = 'Dice Roller';
    },

    showCard: function() {
        document.getElementById('card-img').src = 'img/cards/green_back.png';
        document.getElementById('item-title').innerText = 'Card Picker';
    },

    rollDice: function() {
        var die1Number = Math.floor((Math.random() * 6) + 1);
        var die1Src = 'img/dice/die' + die1Number + '.png';

        var die2Number = Math.floor((Math.random() * 6) + 1);
        var die2Src = 'img/dice/die' + die2Number + '.png';

        document.getElementById('die1-img').src = die1Src;
        document.getElementById('die2-img').src = die2Src;
    },

    pickCard: function() {
        var cardNumber = Math.floor((Math.random() * 52) + 1);
        var imgSrc = 'img/cards/card' + cardNumber + '.png';
        document.getElementById('card-img').src = imgSrc;
    },

    submitForm: function() {
        // Phonegap vibrate plugin to give submit feedback
        navigator.vibrate(3000);
        navigator.notification.alert('We appreciate hearing from you!', null, 'Thanks!');
    },

    openBrowser: function() {
        var ref = cordova.InAppBrowser.open('https://boardgames.com', '_blank', 'location=yes');
    }
};
